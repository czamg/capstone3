import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import './Cards.css';

export default function Cards() {
  return (
    <div className='cards'>
      <h1>FEATURES</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/img-1.jpg'
              text='Explore the new items for the month of August'
              label='Trending'
              path='/products'
            />
            <CardItem
              src='images/img-2.jpg'
              text='Design your island with the seasonal items'
              label='Trending'
              path='/products'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/img-3.jpg'
              text='Tools for terraforming'
              label='In Stock'
              path='/products'
            />
            <CardItem
              src='images/img-4.jpg'
              text='Materials for your dream DIY items'
              label='In Stock'
              path='/products'
            />
            <CardItem
              src='images/img-5.jpg'
              text='Grow more fruits on your island'
              label='In Stock'
              path='/products'
            /> 
            <CardItem
              src='images/img-6.jpg'
              text='Design your island with these furnitures'
              label='In STock'
              path='/products'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}