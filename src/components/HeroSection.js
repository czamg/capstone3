import React from 'react';
import '../App.css';
import { Button } from './Button';
import './HeroSection.css';

export default function HeroSection() {
  return (
    <div className='hero-container'>
      <video src='/videos/video-1.mp4' autoPlay loop muted />
      <h1>NOOK'S CRANNY</h1>
      <p>Shop The Nook Way</p>
      <div className='hero-btns'>
        <Button
          className='btns'
          buttonStyle='btn--outline'
          buttonSize='btn--large'
        >
          GET STARTED
        </Button>
        <Button
          className='btns'
          buttonStyle='btn--primary'
          buttonSize='btn--large'
          onClick={console.log('hey')}
        >
          SHOP HERE
        </Button>
      </div>
    </div>
  );
}





/*import React from 'react';
import '../App.css';
import { Button } from './Button';
import './HeroSection.css';

function HeroSection() {
	return (
		<div className='hero-container'>
			<image src="/images/image-2.jpg" />
			<h1>TOM NOOK</h1>
			<p>Island Provider</p>
			<div className="hero-btns">
				<Button className='btns' buttonStyle='btn--outline'
				buttonSize='btn--large'>
				EXPERIENCE ISLAND LIFE
				</Button>
				<Button className='btns' buttonStyle='btn--large'
				buttonSize='btn--large'>
				SHOP THE NOOK WAY <i className='far fa-play-circle' />
				</Button>
			</div>
		</div>

		)
}*/