//to make a component on our ReactApp we must import the core React module first
import React,{useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
/*import react-bootstrap components here*/
import {Container} from 'react-bootstrap'
import {Nav, Navbar, NavDropdown} from 'react-bootstrap'

/*import react-router-dom components*/
import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

/*import our components here*/
/*import Sample from './components/Sample'
import Profile from './components/myProfile'*/
import NavBar from './components/NavBar'
import Banner from './components/Banner'
import Highlights from './components/Highlights'
import Product from './components/Product'
import Register from './pages/register'
import Login from './pages/login'
import Home from './pages/home'
import Products from './pages/products'
import NotFound from './pages/notFound'
import AddProduct from './pages/addProduct'

//import course data
import products from './data/productsData'

//import provider
import {UserProvider} from './userContext'



function App(){


  const [user,setUser] = useState({

    email: localStorage.getItem('email'),
    isAdmin: JSON.parse(localStorage.getItem('isAdmin'))


  })

  console.log(user)

/*  let nameSample = "Michael Jordan"*/

function unsetUser(){

  //.clear() method for localStorage allows us to clear out the items in our localStorage
  localStorage.clear()

}
  return(

      <>
        <UserProvider value={{user,setUser,unsetUser}}>
          <Router>
          <NavBar />
               <Container>
                  <Switch>
                    <Route exact path="/products" component={Products} />
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/addProduct" component={AddProduct} />
                    <Route component={NotFound} />
                  </Switch>
               </Container>
          </Router>
        </UserProvider>
      </>

    )
}

export default App; //to expose our App.js as component to our ReactJS App


