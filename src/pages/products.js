import React,{useState,useEffect,useContext} from 'react'
//import '../App.css';

import {Table,Button} from 'react-bootstrap'
import {Row, Col} from 'react-bootstrap'
import {Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import Swal from 'sweetalert2'

/*components*/
import Banner from '../components/Banner'
import Product from '../components/Product'

import products from '../data/productsData'

import UserContext from '../userContext'

export default function Products(){

	//get the global user state from User Context.
	const {user} = useContext(UserContext)

	//state to store our courses
	const [allProducts,setAllProducts] = useState([])
	const [activeProducts,setActiveProducts] = useState([])
	const [update,setUpdate] = useState(0)

	useEffect(()=>{

		fetch('http://localhost:4000/api/products/adminallproducts')
		.then(res => res.json())
		.then(data => {

			/*console.log(data)*/

			//all courses
			setAllProducts(data.data)

			let productsTemp = data.data

			/*temporary array to hold filtered items. only active courses*/
			let tempArray = productsTemp.filter(product => {

				console.log(product);
				//filter will return items that pass the condition into the new array.
				return product.isActive === true

			})

			//only active courses
			setActiveProducts(tempArray)

		})

	},[update])

	/*console.log(activeCourses)*/


	

	/*our course components should only show our active course*/
	let productComponents = activeProducts.map(product => {

		return (

			<Product key={product._id} productProp={product} />

			)

	})

	function archive(productId){

		fetch(`http://localhost:4000/api/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			/*refreshing page*/
			/*window.location.replace('/courses')*/
			/*re-render the component and re-run the useEffect which fetches our items*/
			setUpdate({})


		})

	}

/*	console.log(update)*/

	function activate(productId){

		fetch(`http://localhost:4000/api/products/activate/${productId}`,{

			method: 'PUT',
			headers: {

				'Authorization': `Bearer ${localStorage.getItem('token')}`

			}

		})

		.then(res => res.json())

		.then(data => {

			console.log(data)
			setUpdate({})


		})



	}





	//console.log(typeof null)
	//map out all courses to create rows for our tables:
	let productRows = allProducts.map(product => {

		/*console.log(course)*/

		return (

				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.productName}</td>
					<td className={product.isActive ? "text-success": "text-danger"}>{product.isActive ? "Active":"Inactive"}</td>
					<td>
						{
							product.isActive
							?
							<Button variant="danger" className="mx-2" onClick={()=>archive(product._id)}>Archive</Button>
							:
							<Button variant="success" className="mx-2" onClick={()=>activate(product._id)}>Activate</Button>

						}
					</td>
				</tr>


			)

	})


	let bannerContent = {

		title: "ITEMS",
		description: "Affordable Products in the Island",
		label: "Login to Shop",
		destination: "/login",

	}


	console.log(productComponents);

	//Conditionally render the views of the admin and the regular user, The Admin can now view an h1 but the regular user can see our banner and course components
	return (
		user.isAdmin === true
		?
		<>
			<h1 className="text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productRows}
				</tbody>
			</Table>
		</>
		:
		<>
			<Banner bannerProps={bannerContent}/>
			<Container className="mt-5 mb-5">
			    <Row xs={1} md={3} className="mt-5 mb-5">
			    {productComponents}
			    </Row>
			</Container>
		</>

		)

}
