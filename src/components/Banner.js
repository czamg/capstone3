import React from 'react';

//import bootstrap components
import {Jumbotron,Button,Row,Col} from 'react-bootstrap';
// import background from "./images/products_bg.jpg";

//import react router dom components
import {Link} from 'react-router-dom'

/*Link component allows us to navigate within our application. It allow us to switch pages to another page component without refresh.*/

export default function Banner({bannerProps}){

	
	return (

			// <div style={{ backgroundImage: `url(${background})` }}>Example of a DIV element with a background image:</div>

			<Row>
				<Col>
					<Jumbotron>
						<h1>{bannerProps.title}</h1>
						<p>{bannerProps.description}</p>
						<Link to={bannerProps.destination} className="btn btn-primary">{bannerProps.label}</Link>
					</Jumbotron>
				</Col>
			</Row>


		)

}


/*id={bannerProps.class ? bannerProps.class : null}*/