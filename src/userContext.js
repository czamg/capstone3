import React from 'react'

/*
	Context
	
	In reactjs, context is a way to pass data to components without the use of props or without manually props.

	We're going to create a global user state which can be shared and updated by our components.

*/

const UserContext = React.createContext() //siya magiging userContext. 

/* Provider

	Every context object comes with a Provider React component 

	This allows us a way for components to subscribe to change made within a context. This allows us  to have our components access to the values passed in our context.

*/


export const UserProvider = UserContext.Provider

export default UserContext