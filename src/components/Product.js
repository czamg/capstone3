import React, {useState,useEffect} from 'react'

import {Row, Col, Card, Button, CardGroup} from 'react-bootstrap'
import ListGroup from 'react-bootstrap/ListGroup'
import ListGroupItem from 'react-bootstrap/ListGroupItem'

export default function Product({productProp}){

	/*Array destructuring*/
	const [count,setCount] = useState(0)
	const [stocks,setStocks] = useState(30)
	const [isActive,setIsActive] = useState(true)

	/*Will check if stocks is 0, set the isActive state to false and conditionally render the button as disabled every time our component re-renders.*/

	useEffect(()=>{

		if(stocks === 0){
			setIsActive(false)
		}

	},[stocks])


	function buy(){

			setCount(count + 1)
			setStocks(stocks - 1)
		}

	return (

			<Col>
			<Card className="cardProd" style={{ width: '18rem' }}>
				<Card.Body>
					<Card.Title>
						<h2>{productProp.productName}</h2>
					</Card.Title>
					<Card.Text>
						{productProp.description}
					</Card.Text>
					<Card.Text>
						{productProp.bells}
					</Card.Text>
					<Card.Text>
						Quantity: {

							count === 0 

							? <span className="text-danger"></span>

							: <span className="text-success">{count}</span>

						}
					</Card.Text>
					<Card.Text>
						stocks: {

							stocks === 0

							? <span className="text-danger">No more stock available</span>
							: <span className="text-success">{stocks}</span>

						}
					</Card.Text>
					{
						isActive === false

						? <Button variant="primary" disabled>Add to Cart</Button>
						: <Button variant="primary" onClick={buy}>Add to Cart</Button>
					}
					
				</Card.Body>
			</Card>
			</Col>
		)

}
