import React, {useState,useEffect,useContext} from 'react'
import {Form,Button} from 'react-bootstrap'

//import Swal
import Swal from 'sweetalert2'

//import user context
import UserContext from '../userContext'

//import redirect: this component allows redirection from one page to the other
import {Redirect} from 'react-router-dom'

export default function Login(){



	const {user,setUser} = useContext(UserContext)
/*	console.log(user)
	console.log(setUser)
*/
	//States for Email and Password
	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	//State for conditionally rendering our button
	const [isActive,setIsActive] = useState(false)

	//State for redirection
	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{

		if(email !== "" && password !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}


	},[email,password])

	function loginUser(e){

		e.preventDefault()


		fetch('http://localhost:4000/api/login',{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			//console.log(data)

			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Login failed.",
					text: data.message

				})

			} else {
				console.log(data.accessToken)

				/*localStorage is a way for us to store data in our browsers. It can be found in our application tab in the dev tools.*/
				localStorage.setItem('token',data.accessToken)

				fetch('http://localhost:4000/api/profile',{

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}


				})
				.then(res => res.json())
				.then(data => {

					console.log(data)

					localStorage.setItem('email',data.email)
					// localStorage.setItem('isAdmin',data.isAdmin)

					setUser({
						email: data.email,
						isAdmin: data.isAdmin //isAdmin - bool
					})

					//update the willRedirect state after logging in.
					setWillRedirect(true)
					
					Swal.fire({

						icon: "success",
						title: "Successful Log In!",
						text: `Thank you for logging in, ${data.firstName}`

					})

				})

			}

		
		})


		setEmail("")
		setPassword("")
	
	}

	return (

			user.email || willRedirect 
			?
			<Redirect to='/'/>
			:
			<Form onSubmit={e => loginUser(e)}>
				<Form.Group controlId="userEmail">
					<Form.Label>
						Email
					</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e)=>setEmail(e.target.value)} required />
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>
						Password
					</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e)=>setPassword(e.target.value)}required />
				</Form.Group>
				{

					isActive
					?
					<Button variant="primary" type="submit">Submit</Button>
					:
					<Button variant="primary" disabled>Submit</Button>

				}
			</Form>
		)
}
