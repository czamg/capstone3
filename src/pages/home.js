import React from 'react';

import '../App.css';
/*function Home() {
	return (
		<>
		<HeroSection/>
		</>
	);
}
*/

/*import banner and highlights components*/
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import HeroSection from '../components/HeroSection';
import Cards from '../components/Cards';


export default function Home(){

	    // let bannerContent = {
	    //     title: "Nook's Cranny",
	    //     description: "Shop the Nook's Way",
	    //     label: "Shop now",
	    //     destination: "/products"
	    //   }

	    return(
	        <>
		          	<HeroSection/>
		          	<Cards />
	        </>
	    );   
	}
/*

<Banner bannerProps={bannerContent}/>
<Highlights/>



*/