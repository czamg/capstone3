import React, {Fragment} from 'react'; //run the app, and use the ReactJS right away, React is the Core/Backbone of the ReactJS App, instructions for what to display in the User Interface (Browser)
import ReactDOM from 'react-dom'; //assistant to the core library, is the bridge between the instructions and having it display as HTML
import App from './App'; //importing our App component with an Alias 'App' that can be used inside the index.js
//Objective -  display a heading "Hi World!" on our user Interface
let root = document.getElementById('root');

/*We are now rendering App component in the root div.*/
ReactDOM.render(<App />, root)

