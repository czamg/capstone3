import React,{useState, useContext, useEffect} from 'react';
/*useContext hook is a React hook that allows to unwrap our context within our components*/

/*Bootstrap Components*/
/*import components from react-bootstrap:

	These are components which actually creates react elements instead.

*/
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';

/*react-router-dom components

	Link is a component with a to prop which allows to navigate through our pages.

	<Link to="pathInString"/>

*/
import {NavLink, Link} from 'react-router-dom';
import './NavBar.css';
import { Button } from './Button';

//import user context
import UserContext from '../userContext';


export default function NavBar(){

	/*
		useContext is able to unwrap our context object. It will return the data passed as values by a provider (UserProvider in our App.js)

	*/

const {user,unsetUser,setUser} = useContext(UserContext)
/*console.log(useContext(UserContext))*/

function logout(){
	//clearing the localStorage
	unsetUser()

	//update the global user state the clearing the localStorage so we won't need to refresh the page to update our global user state.
	setUser({

		email:null,
		isAdmin:null

	})

	//redirect us to our login page
	window.location.replace('/login')
}
 
 const [click, setClick] = useState(false);
 const [button, setButton] = useState(true);

 const handleClick = () => setClick(!click);
 const closeMobileMenu = () => setClick(false);

const showButton = () => {
	if(window.innerWidth <= 960) {
		setButton(false);
	} else {
		setButton(true);
	}
};


useEffect(() => {

	showButton();

},[])

window.addEventListener('resize', showButton);

	return (
		
		<>
			<nav className='navbar'>
				<div className='navbar-container'>
					<Navbar.Brand as={Link} to="/" onClick={closeMobileMenu}><i class="fas fa-store-alt"></i>Nook's Cranny</Navbar.Brand>
					<div className='menu-icon' onClick={handleClick}>
					<i className={click ? 'fas fa-times' : 'fas fa-bars'} />
					</div>
					<ul className={click ? 'nav-menu active' : 'nav-menu'}>
						<li className='nav-item'>
							<Navbar.Brand as={Link} to="/" className="nav-links" onClick={closeMobileMenu}>
							Home
							</Navbar.Brand>
						</li>
						<li className='nav-item'>
							<Navbar.Brand as={Link} to="/products" className="nav-links" onClick={closeMobileMenu}>
							Products
							</Navbar.Brand>
						</li>
							{
								user.email
								? 
									user.isAdmin
									?	
										<>
											<li className='nav-item'>
												<Nav.Link as={NavLink} to="/addProduct" className="nav-links">Add Product</Nav.Link>
											</li>
											<li className='nav-item'>
												<Nav.Link onClick={logout} className="nav-links">Logout</Nav.Link>
											</li>
											 
										</>
									:
											<li className='nav-item'>
												<Nav.Link onClick={logout} className="nav-links">Logout</Nav.Link>
											</li>
								: 
									<>
										<li className='nav-item'>
											<Navbar.Brand as={Link} to="/register" className="nav-links-mobile" onClick={closeMobileMenu}>
											Sign Up
											</Navbar.Brand>
										</li>
										<li className='nav-item'>
											<Navbar.Brand as={Link} to="/login" className="nav-links" onClick={closeMobileMenu}>
											Login
											</Navbar.Brand>
										</li>
									</>
							}
						
						
					</ul>
					{button && <Button buttonStyle='btn--outline'>SIGN UP</Button>}
				</div>
			</nav>
		</>


		);

}

